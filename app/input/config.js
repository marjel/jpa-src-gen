'use strict';

const fs = require('../core/fs.js');

/**
 * Config "Class"
 */
const Config = function() {

    this.json = null;

    this.entityPackage = '';
    this.sourcePath = '';
    this.destinationDir = '';
    this.templatesDir = '';
    this.packages = null;
    this.classesNames = null;
    this.templatesFilenames = null;
    this.skipColumns = null;

    this.dirs = {

        model: 'model',
        repository: 'repository',
        service: 'service',
        controller: 'controller'

    };

    this.types = [

        'entity',
        'draftEntity',
        'viewEntity',
        'draftRepository',
        'viewRepository',
        'filteredRepository',
        'service',
        'filteredService',
        'controller'

    ];

};
const cp = Config.prototype;

/**
* public
*/
cp.readConfig = function (path) {
    return fs.readJson(path)
        .then(json => {
            this.parseFile(json);
        })
        .catch(err => {
            this.logError(err);
        });
};

cp.resetBeforeNewSession = function () {
    this.dirs = {
        model: 'model',
        repository: 'repository',
        service: 'service',
        controller: 'controller'
    };
    this.entityPackage = this.json.entityPackageName;
    this.sourcePath = this.json.srcPath;
};

/**
 * private
 */
cp.parseFile = function (json) {

    this.json = json;

    this.entityPackage = json.entityPackageName;
    this.destinationDir = json.destinationDir;
    this.templatesDir = json.templatesDir;
    this.sourcePath =
        this.json.srcPath = `./${json.sourceDefaultDir}/${json.sourceFileDefaultName}`;

    this.packages = json.definitions.packages;
    this.classesNames = json.definitions.names;
    this.templatesFilenames = json.definitions.names;

    if(json.skipColumns instanceof Array) {
        this.skipColumns = json.skipColumns;
    }

};

cp.logError = function (e) {
    if(e != null) {
        console.error(e);
    }
};

/**
 * export
 */
module.exports = new Config();

