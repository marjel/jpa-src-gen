'use strict';

const fs = require('../core/fs.js');
const Table = require('./table.js');

/**
 * Source "Class"
 */
const Source = function() {

    this.config = null;
    this.table = new Table();
};

const sp = Source.prototype;

 /** public */
sp.readSource = function (config) {

    this.config = config;

    return fs.readFileAsync(config.sourcePath)
        .then((buffer) => this.table.parse(buffer, this.config.skipColumns));
};


/** export instance */
module.exports = new Source();
