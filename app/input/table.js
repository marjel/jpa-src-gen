'use strict';

const S = require('string');

/**
 * Table "Class"
 */
const Table = function() {
    this.name = null;
    this.columns = [];
};

const tp = Table.prototype;

/** public */
tp.parse = function (data, columnsToSkip) {

    let dta = S(data.toString()).collapseWhitespace().s;
    this.parseName(dta);

    (S(dta).between(' ( ', ' ) ').s).split(', ')
        .filter(col => !columnsToSkip.includes(col.split(' ')[0]))
        .forEach(columnDta => this.parseColumn(columnDta));

    return this;
};


/** private */
tp.parseName = function (data) {
    this.name = S(data).between('create table ', ' ( ').s;
};

tp.parseColumn = function(columnDta) {

    const columnArray = columnDta.split(' ');
    const column = { name: columnArray[0]};
    const typeLength = S(columnArray[1]).between('(', ')').s;

    if(typeLength != null) {
        let parsedLength = parseInt(typeLength);
        if(!isNaN(parsedLength)) {
            column.length = parseInt(typeLength);
        }
        column.type = columnArray[1].split('(')[0];
    } else {
        column.type = columnArray[1];
    }
    column.notNull = columnArray.length === 4;
    this.columns.push(column);
};

/** export "class" */
module.exports = Table;