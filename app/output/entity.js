'use strict';

const fs = require('../core/fs.js');
const S = require('string');
const typeMap = require('./type-map.js');

/**
 * Entity "Class"
 */
const Entity = function() {
    this.table = null;
    this.properties = [];
    this.methods = [];
};

const ep = Entity.prototype;

/** public */
ep.init = function (table) {

    this.table = table;

    this.className = null;
    this.packageName = null;
    this.fileName = null;

    return this;
};

ep.resetBeforeNewSession = function () {

    this.table = null;

    this.properties = [];
    this.methods = [];
};


/** private */

ep.getColumnAnnotation = function(column) {

    let col = '\t\t@Column(name = ';
    col += column.name;
    col += column.length != null ? `, length = ${column.length}` : '';
    col += `, nullable = ${column.notNull})`;

    return col;
}

ep.createProperty = function (column) {

    let property = `${this.getColumnAnnotation(column)}\n`;
    property += `\t\tprivate ${typeMap[column.type]} ${S(column.name).camelize().s};\n`;
    this.properties.push(property);

};

ep.createMethod = function (column) {

    let getterName = column.type === 'bit' ? `is_${column.name}` : `get_${column.name}`;
    let setterName = `set_${column.name}`;

    let getter = `\t\tpublic ${S(getterName).camelize().s}() {\n`;
    getter += `\t\t\treturn this.${S(column.name).camelize().s};\n}\n`;

    let setter = `${getter}\n\t\t${S(setterName).camelize().s}(${typeMap[column.type]} ${S(column.name).camelize().s}) {\n`;
    setter += `\t\t\tthis.${S(column.name).camelize().s} = ${S(column.name).camelize().s};\n}\n`;

    let getterSetter = `${getter}\n${setter}`;

        this.methods.push(getterSetter);


};

ep.createForColumn = function (column) {

    this.createProperty(column);
    this.createMethod(column);

};

ep.createBody = function () {

    const columns = this.table.columns;
    columns.forEach((column) => this.createForColumn(column));

};

/** export class */
module.exports = Entity;