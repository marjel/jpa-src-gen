'use strict';

const fs = require('../core/fs.js');
const Entity = require('./entity.js');

/**
 * Controller "Class"
 */
const Controller = function() {
    this.config = null;
    this.entity = null;
};

const cp = Controller.prototype;

/** public */
cp.init = function (config) {

    this.config = config;

    if(config.entityPackage != null && config.entityPackage !== '') {
        config.dirs.model += '/' + config.entityPackage;
        config.dirs.repository += '/' + config.entityPackage;
    }

    const arr = [];
    for(let dir in config.dirs) {
        arr.push(config.dirs[dir]);
    }

     this.createBaseDirs(arr);

    this.entity = new Entity();
    // this.entity.init()

    return this;
};

cp.clearDestination = function () {
    /*
    * todo: for now please remove manually
    * */
    return this;
};


/** private */
cp.createBaseDirs = function (data) {
    fs.createDirs(data, this.config.destinationDir);
};


/** export instance */
module.exports = new Controller();