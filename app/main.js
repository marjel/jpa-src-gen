'use strict';

const terminal = require('./view/terminal.js');
const config = require('./input/config.js');
const source = require('./input/source.js');
const controller = require('./output/controller.js');

const parsed = './config/settings.json';
const defaultConfigPath = `${parsed}`;

config.readConfig(defaultConfigPath)
    .then(d => terminal.init());

terminal.addListener('ready', (e) => source.readSource(config)
    .then(d => controller.init(config))
        .then(d => terminal.asyncReadLine(2))
            .then(d => config.resetBeforeNewSession()));

terminal.addListener('sourcePath', (data) => config.sourcePath = terminal.sourcePath);
terminal.addListener('packageName', (data) => config.entityPackage = terminal.packageName);
