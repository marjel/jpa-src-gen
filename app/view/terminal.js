'use strict';

const events = require('events');
const readline = require('readline');

const inlineConsole = require('single-line-log').stdout;

const td = require('timedown');
const timer = td();

/**
 * Terminal "Class"
 */

const Terminal = function() {

    /** public properties */
    this.sourcePath = null;
    this.packageName = null;

    /** command line configuration */
    this.readline = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    /** console flags */
    this.pointer = 0;
    this.isExiting = false;

    this.timeout = null;

    /** std private messages */
    this.enterMessage =
        'Jeśli chcesz zakończyć sesję - wciśnij "Q"(quit)\nŻeby generować wciśnij "Enter"... lub zaczekaj 5 sekund\n';
    this.exitMessage = 'Chcesz zakończyć sesję? (T/N)\n';
    this.alreadyExitingMessage =
        'Wcześniej próbowałeś zakończyć sesję. Jeśli chcesz teraz wyjść z aplikacji wciśnij "T"\n';
    this.goodByeMessage = 'Generator kodu JPA zakończył działanie';
    this.successMessage = 'Wygeneowano klasy JPA';
    this.useDefaultConfigPathMessage =
        'Nie podałeś ścieżki do definicji SQL. Zostanie użyta ścieżka z pliku konfiguracyjnego (config/settings.json)';

    this.useDefaultPackageMessage ='Nie podałeś nazwy pakietu. Klasy zostaną dodane do bieżącego pakietu głównego';
    this.howToFinishMessage = 'Jeśli chcesz zakończyć sesję - wciśnij "Q"(quit)';
    this.pressEnterOrWaitMessage = 'Żeby generować wciśnij "Enter"... lub zaczekaj';

    /** flow messages */
    this.messages = [
        '1. Podaj ścieżkę pliku z tabelą sql ("Enter" - domyślna lokalizacja "source/def.sql"): ',
        '2. Podaj nazwę pakietu dla encji: ',
        '\nPodaj ścieżkę do kolejnej definicji tabeli sql ("Enter") lub zakończ działanie aplikacji ("Q")\n'
    ];

    this.emitter = new events.EventEmitter();
};

const tp = Terminal.prototype;


/** public methods */

tp.init = function () {
    this.clearConsole();
    this.initialCountdown = timer.ns('initialCountdown', '5s');
    this.asyncReadLine(0, true);
    this.pointer = 0;
    this.setFirstView();
};

tp.addListener = function (eventName, listenerFunction) {
    this.emitter.addListener(eventName, listenerFunction);
};

tp.removeListener = function (eventName, listenerFunction) {
    this.emitter.removeListener(eventName, listenerFunction);
};

tp.dispatch = function (eventName, data) {
    this.emitter.emit(eventName, data);
};

/** private methods */

tp.displayFirstMessage = function () {
    this.clearConsole();
    this.pointer = 0;
    this.pointerFlow(0);
};

tp.setFirstView = function () {
    this.clearConsole();
    inlineConsole(this.howToFinishMessage);
    this.initialCountdown
        .on('tick', (time) => {
            inlineConsole.clear();
            inlineConsole(`${this.pressEnterOrWaitMessage} ${Math.round(time.ms/1000)} sekund\n`);
        });
    this.initialCountdown
        .on('end', () => this.asyncReadLine(0, true));
    this.initialCountdown.start();
};

tp.clearConsole = function () {
    console.clear();
    /*inlineConsole.clear();*/
};

tp.asyncReadLine = function (mess, initial) {
    if(initial === true) {
        this.displayFirstMessage();
    } else {
        this.initialCountdown.stop();
    }
    let messInteger = mess != null ? parseInt(mess) : NaN;
    if(!isNaN(messInteger)) {
        this.pointer = messInteger !== this.pointer ?
            messInteger >= this.messages.length - 1 ? 0 : messInteger + 1 : messInteger + 1;
    }
    if(this.pointer === this.messages.length-1) {
        this.pointer = 0;
    }
    let message = mess != null ? (!isNaN(messInteger) && messInteger < this.messages.length)
        ? this.messages[messInteger] : mess : this.messages[this.pointer];

    this.readline
        .question(message, (line) => {
            if (line){
                /** type to command line and confirm by Enter */
                if(line.toLowerCase() === 't') {
                    if(this.isExiting) {
                        this.clearConsole();
                        this.asyncReadLine(this.goodByeMessage);
                        this.readline.close();
                        process.exit(0);
                    } else {
                        this.pointerFlow(this.pointer);
                    }
                } else if(line.toLowerCase() === 'q') {
                    if(!this.isExiting) {
                        this.clearConsole();
                        this.isExiting = true;
                        this.asyncReadLine(this.exitMessage);
                    } else {
                        this.asyncReadLine(this.alreadyExitingMessage);
                    }
                } else {
                    if(this.isExiting) {
                        this.isExiting = false;
                        this.pointer = 0;
                        line = null;
                    }
                    /** just normal flow :) */
                    this.pointerFlow(this.pointer, line);
                }
            }else{
                /** just Enter pressed */
                this.initialCountdown.stop();
                this.pointerFlow(this.pointer);
            }
            /** pointer incrementation */
            this.pointer++;
        });
};

tp.pointerFlow = function (pointer, text) {
    switch (pointer) {
        case 0:
            this.clearConsole();
            this.asyncReadLine();
            break;
        case 1:
            if(text != null && (this.sourcePath !== text)) {
                this.sourcePath = text;
                this.dispatch('sourcePath');
            } else {
                console.info(this.useDefaultConfigPathMessage);
            }
            this.asyncReadLine();
            break;
        case 2:
            if(text != null && (this.sourcePath !== text)) {
                this.packageName = text;
                this.dispatch('packageName');
            } else {
                console.info(this.useDefaultPackageMessage);
            }
            this.dispatch('ready');
            break;
    }
};

/** export instance */
module.exports = new Terminal();