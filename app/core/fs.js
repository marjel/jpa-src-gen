'use strict';

const path = require('path');
const fs = require('fs-extra');

fs.readFileAsync = function (filename) {
    return new Promise(function (resolve, reject) {
        try {
            fs.readFile(filename, function(err, buffer){
                if (err) reject(err); else resolve(buffer);
            });
        } catch (err) {
            reject(err);
        }
    });
};

fs.writeClass = function (classData, name, destination) {
    let outputPath = path.join(destination, `${name}.java`);
    return fs.outputFile(outputPath, classData);
};

fs.createDir = function (name, destination, callback) {
    let dir = path.join(destination, name);
    return fs.ensureDir(dir)
        .then(callback instanceof Function ? () => callback() : null)
        .catch(err => console.error(err));
};

fs.createDirs = function (names, destination, callback) {

    return names.forEach((name, index) => {
            fs.createDir(name, destination, (index === names.length-1) ?
                callback : function(){});
    });
};

module.exports = fs;