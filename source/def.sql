create table example_of_entity
(
	id decimal(18) identity
		constraint PK_account
			primary key,
	example_of_entity_number varchar(50),
	decimal_example decimal(18),
	decimal_not_null decimal(18) not null,
	varchar_50_not_null varchar(50) not null,
	varchar_12_example varchar(12),
	text_255_example varchar(255) not null,
	int_not_null int not null,
	int_example int,
	small_int_example smallint,
	date_example datetime not null,
	char_8_example char(8),
	example char(8),
	bit_not_null bit not null,
	bit_example bit,
	varchar_max_example varchar(max),
	user_create char(8),
	date_create datetime,
	user_update char(8),
	date_update datetime,
	user_approve char(8),
	date_approve datetime
)
go

create unique index IX_account
	on account (example_of_entity_number)
go

